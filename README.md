Un autoconectador a wifi para OpenWRT/LEDE/MallaCritica

# Uso

```bash
kawaii [wlan0] [kawaii.csv]
```

El primer argumento es la interfaz, por defecto `wlan0`.  **Tiene que
corresponder con la primera interfaz configurada de
`/etc/config/wireless`.**

El segundo es un archivo CSV con las conexiones que conocemos.  Hay que
seguir este formato:

```
SSID;cifrado;MAC (opcional);contraseña (opcional);
```

Donde `SSID` es la columna con el nombre de la red, `cifrado` es el tipo
de cifrado de la red, según opciones de OpenWRT (`none`, `wep`, `psk`,
etc.), `MAC` una MAC específica que queramos usar (por defecto se genera
una aleatoria) y la contraseña de la red, si tuviera.

En base a esto, `kawaii` va a intentar escanear la red actual,
ordenarlas por mejor señal y conectarse una por una si las encuentra en
el archivo de redes conocidas.  Luego hace algunas pruebas básicas de
Internet y si todo funciona bien, ya estamos conectadas :)
